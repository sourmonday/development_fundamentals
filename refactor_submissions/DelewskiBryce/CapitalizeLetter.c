/*
The function CapitalizeLetter(str) takes a string an capitalizes the first letter of each word, delimited by any non-alphabet character.
*/
#include <stdio.h>

enum ALPHABET {
    A=65,
    B=66,
    C=67,
    D=68,
    E=69,
    F=70,
    G=71,
    H=72,
    I=73,
    J=74,
    K=75,
    L=76,
    M=77,
    N=78,
    O=79,
    P=80,
    Q=81,
    R=82,
    S=83,
    T=84,
    U=85,
    V=86,
    W=87,
    X=88,
    Y=89,
    Z=90
} letters;

int length_of_string(const char* s);

char* CapitalizeLetter(char str[]) {
  int l = length_of_string(str);
  int position = 0;
  for(; l > 0; l--)
  {
      if (((position==0)
         || ((str[position-1] < A  || str[position-1] > Z)
            && (str[position-1] < A + 32 || str[position-1] > Z + 32)))
         && str[position] >= A + 32 && str[position] <= Z + 32){
           for(int letter=0; letter < (Z - A); letter++) {
             if (letter + A + 32 == str[position]){
               str[position] = letter + A;
             }
           }
      }
      position = position + 1;
  }
  
  return str;
}

int main(int argc, char** argv) {
  if (argc != 2) return -1;
  printf("%s\n", CapitalizeLetter(argv[1]));
  return 0;
    
}

int length_of_string(const char *str)
{
  if(*str){
    return 1 + length_of_string(str+1);
  }
  return 0;
}

