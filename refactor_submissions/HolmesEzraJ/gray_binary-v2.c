#include <stdio.h>

#define ERROR_SUCCESS           0
#define ERROR_NULL_POINTER      -2
#define ERROR_INVALID_INPUT     -3

/******************************************************************************
* function: d2b
* 
* Purpose: This function converts a decimal integer type to its binary 
*          equivalent and stores the result in an integer array. Upon 
*          completion, the size of the new binary number is returned.
* 
* Input Parameters:
*   <elementswithinbuffer>      An integer representing the size of the output buffer.
*   <n>    The decimal number for binary convertion.
* 
* Output Parameters:
*   <abuffertostoreresults>  An integer array storing the binary equivalent of input number; the first index represents the least significant bit.
* 
* Return Values:
*   [integer >= 0]  The size of the binary number stored in output buffer.
******************************************************************************/

int d2b(int abuffertostoreresults[], int elementswithinbuffer, int n)
{
int nd = 0; // Stores the size of the binary version of number
int x = n; // Preserve the original number

while (x > 0 && nd < elementswithinbuffer){ /*Convert the decimal number to its binary equivalent*/ abuffertostoreresults[nd++] = x % 2;x /= 2;}
return nd;
}

/******************************************************************************
* function: b2d
* 
* Purpose: This function converts a binary number, represented as an integer array, to its decimal equivalent. Upon success, the decimal equivalent is returned.
* 
* Input Parameters:
*   <q>    An integer array representing a binary number; the first index represents the least significant bit.
*   <z>      An integer representing the size of the output buffer.
* 
* Return Values:
*   [integer >= 0]  The decimal equivalent of the input binary number.
******************************************************************************/

int b2d(int q[], int z)
{
    int x = 1; // Stores the current binary place during convertion
    int y = 0; // Stores the resulting decimal equivalent of input binary
    
    for (int index = 0; index < z; index++){ // Convert the input binary number to its decimal equivalent
        y += *(q + index) * x;
        x <<= 1;}
    return y; // The final converted decimal number
}

/******************************************************************************
* function: gray_to_binary
* 
* Purpose: This function converts a gray binary number, represented as an 
*          integer array, to its binary equivalent. Upon success, ERROR_SUCCESS
*          is returned representing a successful operation.
* 
* Input Parameters:
*   <gray_bin>    An integer array representing a binary number; the first index represents the least significant bit.
*   <size>      An integer representing the size of the output buffer.
* 
* Return Values:
*   [ERROR_SUCCESS]  Indicates the operation completed successfully.
******************************************************************************/

int gray_to_binary(int gray_bin[], int size)
{
    for (int index = size - 1; index >= 0; index--){if (index - 1 >= 0){gray_bin[index - 1] = gray_bin[index] ^ gray_bin[index - 1];}}return ERROR_SUCCESS;
}

int main()
{
    int a = 9, tests[] = {4, 5, 15, 1356, 228934, 837665, 200, 90677983, 3333};
    
    while (--a >= 0)
    { // Process each test case
        int bufferforbinaryversionofnumber[(sizeof(int) * 8)];
        int sizeofcalculatedbinarynumber = d2b(bufferforbinaryversionofnumber, (sizeof(int) * 8), *(tests + a));
        gray_to_binary(bufferforbinaryversionofnumber, sizeofcalculatedbinarynumber);
        printf("%d = %d\n", tests[a], b2d(bufferforbinaryversionofnumber, sizeofcalculatedbinarynumber));
    }
	return ERROR_SUCCESS;
}
