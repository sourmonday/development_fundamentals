# WinDbg Primer: Introducing the Windows Debugger

### Objective

Students will be introduced to the basics of debugging and WinDbg. Because students are expected to be ready for Journeyman level training, this primer serves to introduce "start points" to begin personal research rather than serving as a through WinDbg course.

### Key Results

* After reviewing included content and third-party resources (not those in additional resources), students shall be able to view the stack, examine loaded modules, & execute basic debugger commands (breakpoint, run, continue, etc.) on Windows
* Students shall be able to view Windows user mode data structures and tell basic facts about the process(es) under analysis
* Students shall have be informed of additional third-party resources to continue research and learning

### Key Assumptions

* Students are familiar with x86 Assembly and C/C++

# Table of Contents
1. [What is Debugging](#what-is-debugging)
2. [What is a Debugger](#what-is-a-debugger)
1. [x86 Assembly Resources](#asm-rscs)
1. [WinDbg Basic Operation](#windbg-op)
1. [Windows User-Mode Objects and Data Structures](#windows)
1. [Debugging Strategies](#debug-strats)
1. [Guided Exercises](#exercises)
1. [Additional Resources](#resources)

#### What is Debugging <a name="what-is-debugging"></a>

[Debugging](https://www.techopedia.com/definition/16373/debugging) is the process of locating and removing computer program bugs, errors, or abnormalities.

#### What is a Debugger <a name="what-is-a-debugger"></a>

In order to achieve the process of debugging, software developers have access to debugging tools. One of these kinds of tools is known as a debugger. A debugger allows you to stop or halt the program according to specific conditions and to control program flow manually. 

#### x86 Assembly Resources <a name="asm-rscs"></a>

Microsoft has [an annotated disassembly](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/annotated-x86-disassembly) for review.

[And a free book! Who doesn't love free stuff!](http://www.ic.unicamp.br/~pannain/mc404/aulas/pdfs/asm%20-%20windows%20assembly%20language%20and%20systems%20programming.pdf)

When in doubt, reference the [Intel Software Developer Manuals!](https://software.intel.com/en-us/articles/intel-sdm)

#### WinDbg Basic Operation <a name="windbg-op"></a>

If there is only one thing to take away from this post, it is to always check out the provided Help section on the Tool Menu. Go to Help > Contents and you will be able to find very detailed descriptions / documentation on everything that you ever wanted to know about operating the debugger. 

Conveniently, the first page also shows a very great start point called Getting Started with Windows Debugging, the web version of which can be found at [this link.](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/getting-started-with-windows-debugging)

User-mode debugging is covered [here.](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/getting-started-with-windbg)
Other than that, this table contains a quick reference of useful commands that can get you started:

| Command | Command Action                                          | Additional Info |
|---------|---------------------------------------------------------|-----------------|
| k       | View call stack         | In some cases, the k commands will not be able to reconstruct the stack. You will need to manually reconstruct it.|
| d       | Display contents of memory  | |
| r       | View registers |        |
| x       | Examine symbols |       |
| b       | Set breakpoint          | In Windows, a module may or may not be loaded yet. You may set a deferred breakpoint on a module that hasn't been loaded yet. Deferred breakpoints are the preferred method of setting breakpoints |
|         |                         | Example: *bu ole32!CoInitializeEx*              |
| g       | go                      | |
| lm      | List loaded modules     | |
| ~       | View threads            | |
| .reload | Delete all symbol info and reload as needed | |
| .sympath | Set symbol path        | |

Other Cheat Sheets
* [Cheat Sheet 1](http://windbg.info/doc/1-common-cmds.html)
* [Cheat Sheet 2](https://theartofdev.com/windbg-cheat-sheet/)
* [Cheat Sheet 3](https://bsodtutorials.files.wordpress.com/2014/11/windbg-cheat-sheet.pdf)

#### Windows User-Mode Objects and Data Structures <a name="windows"></a>

Windows is full of interesting data structures that you can look at to better understand the process under the hood. For example, the [Process Environment Block (PEB)](https://docs.microsoft.com/en-us/windows/desktop/api/winternl/ns-winternl-_peb) is a data structure that maintains a BeingDebugged variable, which is one anti-analysis technique used by some malware.

You can dump the PEB and the thread version of it, the TEB, by using the debugger extension commands ```!peb``` and ```!teb```.

You might ask what are other data structures that are available for analysis. You can get the current fullest listing by dumping available variables in the Native API dll. Type in the command ```dt ntdll!_*```

![](assets/windbg_ntdll_dump.PNG)

Note that you will have to supply an address to the ```dt``` command in order to actually cast the supplied bytes into your desired data structure format. You will be able to find the addresses you want by going directly to the TEB through the special register (as noted in the Win32 Thread Information Block wiki in Additional Resources) or by walking the PE file.

*Additional References*

* https://en.wikipedia.org/wiki/Process_Environment_Block
* https://en.wikipedia.org/wiki/Win32_Thread_Information_Block
* https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/dt--display-type- 
* https://docs.microsoft.com/en-us/windows/desktop/debug/pe-format
* https://msdn.microsoft.com/en-us/library/ms809762.aspx 

#### Debugging Strategies <a name="debug-strats"></a>

[Sauce: Debugging - The 9 Indispensible Rules for Finding Even the Most Elusive Software and Hardware Problems](https://www.amazon.com/Debugging-Indispensable-Software-Hardware-Problems/dp/0814474578?keywords=debugging+9+indispensable+rules&qid=1539178732&s=Books&sr=1-1-fkmrnull&ref=sr_1_fkmrnull_1)

1. *Understand the System:* “You need a working knowledge of what the system is supposed to do, how it’s designed, and, in some cases, why it was designed that way. If you don’t understand some part of the system, that always seems to be where the problem is.” __Read the F.... Manual__
1. *Make It Fail:* You want to make it fail so that you can look at it, so you can focus on the cause, and so you can tell if you’ve fixed it. *Simulate* the conditions that **stimulate** the failure, but don’t *simulate* the failure itself.
1. *Quit Thinking and Look:* “It is a capital mistake to theorize before one has data. Insensibly one begins to twist facts to suit theories, instead of theories to suit facts.” -- Sherlock Holmes. If you guess at how something is failing, you often fix something that isn’t the bug
1. *[Divide and Conquer](https://en.wikipedia.org/wiki/Divide_and_conquer_algorithm)*: Split up your search space into a good half and bad half and find where the issue is efficiently! 
1. *Change One Thing at a Time:* If you change ten, how can you be sure which of your changes actually fixed the problem?
1. *Keep an Audit Trail:* Write down what you did, in what order, and what happened
1. *Check the Plug:* Have you turned it off and on again? Is it plugged in? Don’t discount the “obvious things” and assume the general requirements. Question your assumptions. Rarely, it might be a problem with your tool. Your tool was built by engineers like you, why would it be any more trustworthy than what you’re building?
1. *Get a Fresh View:* Find your rubber ducky. Try a human rubber ducky.
1. *If You Didn’t Fix It, It Ain’t Fixed:* Confirm the fix is done. Don’t just swap out what you think is wrong and never take the time to check your work!

#### Guided Exercises <a name="exercises"></a>

__TODO:__ Make guided exercises


#### Additional Resources <a name="resources"></a>

1. [WinDbg Slide A to Z](http://windbg.info/download/doc/pdf/WinDbg_A_to_Z_bw2.pdf)
1. [Windows Internals, 7th Edition Part 1](https://www.amazon.com/Windows-Internals-Part-architecture-management/dp/0735684189?keywords=windows+internals&qid=1539181920&sr=8-1&ref=sr_1_1)
1. [Windows Internals, 6th Edition Part 2](https://www.amazon.com/Windows-Internals-Part-Developer-Reference/dp/0735665877/ref=pd_bxgy_14_3?_encoding=UTF8&pd_rd_i=0735665877&pd_rd_r=41eaf5e6-cc99-11e8-b42b-1db580407759&pd_rd_w=W05s9&pd_rd_wg=s2hBO&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=6725dbd6-9917-451d-beba-16af7874e407&pf_rd_r=7QT1CBEHXAVRNKZKYXXS&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=7QT1CBEHXAVRNKZKYXXS)
1. [Inside Windows Debugging (Developer Reference)](https://www.amazon.com/Inside-Windows-Debugging-Developer-Reference/dp/0735662789/ref=pd_sim_14_4?_encoding=UTF8&pd_rd_i=0735662789&pd_rd_r=41eaf5e6-cc99-11e8-b42b-1db580407759&pd_rd_w=pgRy8&pd_rd_wg=s2hBO&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=18bb0b78-4200-49b9-ac91-f141d61a1780&pf_rd_r=7QT1CBEHXAVRNKZKYXXS&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=7QT1CBEHXAVRNKZKYXXS)
1. [Windows Kernel Debugging Fundamentals on Pluralsight](https://app.pluralsight.com/library/courses/windows-debugging-fundamentals/table-of-contents)
1. [Advanced Windows Debugging - Part 1 on Pluralsight](https://www.pluralsight.com/courses/adv-win-debug-part-1)
1. [Advanced Windows Debugging - Part 2 on Pluralsight](https://docs.google.com/document/d/1d33hJWt26cnkqT4ytc0ZjBXkG73JQInbivHGQyBSE7Y/edit#)
1. [Windows Internals on Pluralsight](https://www.pluralsight.com/courses/windows-internals)
