# III. Programming / Analysis Requirements (Master)

## Master (9) Description
A very accomplished programmer in multiple languages. Able to accomplish tasks and direct others well. The equivalent of a senior level engineer. The expectation is to be signed off for this level after approximately 1500-2000 hours of development (TODO: approximately One and a half-year of full-time development). The below are the changes from Apprentice-5 level

