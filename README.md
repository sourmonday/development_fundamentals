# development_fundamentals

# Objective:

* Introduction to Development Fundamentals is a boot-camp style of teaching problem decomposition to general students
who have no prior experience with programming / computer science. 


# Key Results:

* Students shall be introduced to the C programming language.
* Students shall understand the situations in which to use if-else statements
* Students shall understand the situations in which to use for, while, and do-while loops
* Students shall understand the use of C pointers, Structures, Arrays & C-style strings
* Students shall be able to implement low-level (or not low level) networking 
* Students shall be able to interact with the file system using C 
* Students shall be able to debug programs they write or problems provided to them
* Students shall understand x86 Assembly concepts to enable assembly level debugging for C
* Students shall be able to solve complex puzzles using the above concepts
* Students shall be briefly exposed to Python and introduced to the main differences and standard library functions


## Background

The problem set being addressed is the same outlined in [this blog post.](https://blog.ret2.io/2018/09/11/scalable-security-education/)

On this curve, we would like to address the steep areas of the curve, C to memory layout, with the exception of IDA.

![image of wargames curve](pics/wargames_learning_curve.png)

## Syllabus

Currently, IDF is scheduled for 16 weeks.

| Week ## | Topics                                                                   | Prerequisite(s)                                                                               |
|---------|--------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| Week 00 | 1. Introduction to Programming - Concepts and Approaches
|         | 2. If-else statements                                                    |                                                                                               |
|         | 3. Loops                                                                 |                                                                                               |
| Week 01 | 4. Introduction to x86 Assembly                                          |                                                                                               |
|         | 5. Introduction to Debugging                                             |  Introduction to x86 Assembly                                                                 |
| Week 02 | 6. Arrays and Structures                                                 |                                                                                               |
|         | 7. C-style strings                                                       |                                                                                               |
|         | 8. C Basics Puzzle Capstone                                              |                                                                                               |
| Week 03 | 9. C File I/O                                                            |                                                                                               |
|         | 10. C Networking                                                         |  C-style Strings                                                                              |
| Week 04 | 11. Pointers and Function Pointers                                       |                                                                                               |
|         | 12. Stacks (programmatic) and Linked Lists                               |  Pointers and Function Pointers                                                               |
|         | 13. Stacks and Heaps                                                     |                                                                                               |
|         | 14. Trees                                                                |  Arrays and Structures, Stacks (programatic) and Linked Lists, Pointers and Function Pointers |
| Week 05 | 15. Error Handling                                                       |                                                                                               |
|         | 16. Algorithms with Data Structures                                      |  Arrays and Structures                                                                        |
| Week 06 | 17. Algorithms with Data Structures (2)                                  |                                                                                               |
| Week 07 | 18. Applications of Algorithms                                           |                                                                                               |
| Week 08 | 19. Algorithm Crucible                                                   |  Algorithms with Data Structures                                                              |
| Week 09 | 20. Algorithm Crucible Review                                            |                                                                                               |
|         | 21. Python Crash Course                                                  |  none, but as much C knowledge as possible is helpful                                         |
| Week 10 | 22. C Standard Library Calls                                             |                                                                                               |
|         | 23. Re-factoring Legacy Code                                             |                                                                                               |
|         | 24. Code Quality Best Practices                                          |                                                                                               |
| Week 11 | 25. Applications of Algorithms Refresher                                 |                                                                                               |
|         | 26. Algorithm Crucible (2)                                               |  Algorithm Crucible 1                                                                         |
| Week 12 | 27. Capstone Sprint 1                                                    |                                                                                               |
| Week 13 | 28. Capstone Sprint 2                                                    |                                                                                               |
| Week 14 | 29. Capstone Sprint 3                                                    |                                                                                               |
| Week 15 | 30. Capstone Sprint 4                                                    |                                                                                               |

## Additional Units

If there is additional time, these are the suggested additional units:

| Unit ## | Topics                                                    |  Placement    |
|---------|-----------------------------------------------------------|---------------|
| Unit 00 | [Learning How to Learn Course](https://www.coursera.org/learn/learning-how-to-learn/#syllabus)  | Before Week 00 |
| Unit 01 | Writing Testable Code and Tests                         | Before Refactoring |


***

## Class Requirements

The entire course will be taught using Linux (probably Ubuntu). The class will use a
text editor with syntax plugin-ins and use system tools like gcc and gdb to build/compile,
link and debug. 

The instructor may choose to demonstrate certain concepts from the beginning using an IDE in
order to more easily illustrate his/her point.

At the beginning of the Algorithm Crucible (Week 08), the class shall be introduced to an
IDE to allow for quicker development. 

## Comments on Organization Style

Students are full-time, 8 hours a day in this class. Traditionally, a one semester class 
(at least in my alma mater) consists of 40 hours of actual class time. Electronics 200 level
and other like complex courses were double periods, meaning 80 hours of class time. To scale
expectations, each work-week is expected to have 40 hours minimum of work. Scaling at worst-case
considerations, each two weeks can be considered approximately half a year of material.

**TODO**: With the above comment, it feels like there can be additional compression in the course.

Due to concept immersion, you should expect the level of competence to rise over time. The pace
of later weeks should reflect the ability to take on increased workload. While trying to operate at
a high pace, slack time is built in with low tempo lessons in between crucibles (like Python and 
re-factoring).
