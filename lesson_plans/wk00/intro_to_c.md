## Tell Me a Story About Punch Cards

Don't you hate it when those old timers at your development shop tell you about the crappy old days when programming was done with punch cards and you had to get everything perfect the first time? What a pain that must have been.

What's that? Oh. You're not a developer, but you want to be?

Well, awesome! And why wouldn't you want to be? 

[Building almost every technology under the sun... and it's all theoretically possible with just one command!](https://www.cl.cam.ac.uk/~sd601/papers/mov.pdf)

Right... well, that's not actually what we do... and, yes, there's actually quite a bit more than just *one* instruction...


But, anyways, excited to have you! But before that, let me tell you a story about C...

## Excerpt From Once Upon an Algorithm

*What is computation?* This question lies at the core of computer science. This chapter provides an answer -- at least a tentative one -- and connects the notion of computation to some closely related concepts

...

The first view, *computation solves problems,* emphasizes that a problem can be solved through computation once it is suitably represented and broken down into subproblems.

...

However, the problem-solving perspective leaves out some important aspects of computation. A closer look at the differences between computation and problem solving leads to a second view, computation is algorithm execution. An algorithm is a precise description of computation and makes it possible to automate and analyze computation.

...

The key to harnessing computation lies in grouping similar problems into one class and designing an algorithm that solves each problem in that class.

[Erwig, Martin. Once Upon an Algorithm: How Stories Explain Computing (The MIT Press) (p. 19). The MIT Press. Kindle Edition.](https://www.amazon.com/Once-Upon-Algorithm-Stories-Computing-ebook/dp/B074TX351D/ref=sr_1_1?ie=UTF8&qid=1539376296&sr=8-1&keywords=once+upon+an+algorithm)


## What?

So what that means is, our job here is to build patterns that solve entire problem classes and use these individual patterns to build bigger and more intricate patterns! That is, bricks-walls-houses (yes, I am still in the Three Little Pigs era).

## Let's Just Get to the C

When you first create a C executable in an IDE, you might get some auto-generated code like this

```c
#include <stdio.h>

int main() {
    printf("Hello, World!\n");
    return 0;
}
```

Everything after "int main()" and in between the curly braces is what is called a function. At a high level, this just means a set of directions for the computer.

Critically, you can call other functions from functions:

```c
#include <stdio.h>

void so_much_printing(int recurse_cnt)
{
    if (recurse_cnt >= 0)
    {
        printf("Printing is the only job for me!\n")
    }
    so_much_printing(recurse_cnt - 1);
}

int main() {
    printf("Hello, World!\n");
    so_much_printing(5);
    return 0;
}
```

```c 
#include <stdio.h>

void print_more()
{
    printf("Seriously, this is all I can do.");
}

void so_much_printing(int recurse_cnt)
{
    if (recurse_cnt >= 0)
    {
        printf("Printing is the only job for me!\n")
    }
    so_much_printing(recurse_cnt - 1);
    print_more();
}

int main() {
    printf("Hello, World!\n");
    so_much_printing(5);
    return 0;
}
```

## Now You Try!

Just like how "stdio.h" is included in the above examples, include "math.h" and print some functions that computes the square root and prints out your results. You might have to Google how to print a number using printf. 

5-10 minutes

Before you move on to the next chapter, take another 5-10 minutes and review [this.](https://www.cprogramming.com/tutorial/c/lesson1.html)