Students who complete this course should have a firm understanding of how an operating system works, what tasks it performs as well as how information is passed, stored and managed. 

## Topics

|Week # |Topics Intro to Operating Systems                          |  Additional material 
|-------|----------------------------------------------------------|---------------------------|
|Week 0 | Binary, Octal, Hexidecimal |
|       | Bitmaps, XOR, etc |
|       | The CPU |
|       | Memory |
|Week 1 | Memory management |
|       | Peripherals and I/O (BIOS) |
|       | Virtual Memory vs. Physical Memory (cache, ROM, RAM) |
|       | File Systems |
|Week 2 | Processes (hierarchy), Fork and Exec |
|       | Threads | Dynamic Linking
|Week 3 | Scheduling, context switching, deadlocks and semaphores | Startup Process*
|       | System Calls
|Week 4 | Networking
|       | Mass Storage Structures (databases) 

* What is the difference between Startup Process and BIOS stuff??


|Topics                          | Course Construction status | Suggested resources|
|-----------------------------------------------------------|---------------------------|-----------------------------------------------------------------------------------|
| Binary, Octal, Hexidecimal | yes | 
| Bitmaps, XOR, etc | no | https://codescracker.com/operating-system/memory-management-with-bitmaps.htm 
| The CPU | yes |
| Memory | yes |
| Memory management | no | Modern Operating Systems Andrew S. Tanenbaum, Herbert Bos; chapter 3
| Peripherals and I/P (BIOS) | partial | https://courses.engr.illinois.edu/cs241/sp2014/lecture/38-io.pdf 
| Virtual Memory vs. Physical Memory (cache, ROM, RAM) | yes |
| File Systems | partial | Modern Operating Systems Andrew S. Tanenbaum, Herbert Bos; chapter 4 
| Processes (hierarchy), Fork and Exec | yes |
| Threads | no | https://learn.saylor.org/course/cs401
| Scheduling, context switching, deadlocks and semaphores | no | https://learn.saylor.org/course/cs401
| System Calls | yes |
| Virtual Machines | no | http://codex.cs.yale.edu/avi/os-book/OS9/toc-dir/toc.pdf; chapter 16
| Networking | partial | 
| Distributed Systems | no | http://codex.cs.yale.edu/avi/os-book/OS9/toc-dir/toc.pdf chapter 17
| Mass Storage Structures (databases) | no |
| Dynamic Linking | yes | 
| Startup Process | no |

