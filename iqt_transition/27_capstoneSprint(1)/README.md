section 27: Capstone Sprint (1) 

## Additional Information

* Our current C/C++ capstone is a variable length capstone
    * Generally 1-3 weeks
* Requirments are as followed:
    * Teams of 3-4
    * Use of project management
    * Use of version control (git)
    * Use of proper coding style
    * Use of features defined by instructors
    * Must follow theme defined by instructors (generally open to most)
    * Must be approved by instructors
    * Must be a project that can be split between 3-4 people
    * Team leader must be defined, they will be the final say in management