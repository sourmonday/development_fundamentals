section 12: Stacks (pragmmatic) and Linked Lists

* We do not currently go over specifically Stacks, but we do go over Linked Lists
* Some of our labs and live exercises introduce the concept of the stack
* ASM goes deeper into the concept of stack data structure
* We do implement Linked Lists that go into the concept of stack data structures
* We also go over Linked Lists further in ASM