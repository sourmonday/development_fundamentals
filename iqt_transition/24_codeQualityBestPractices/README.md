section 24: Code Quality best practices 

## Additional Information

* This section is spread throughout the existing codebase. 
* Each chapter we have a section called "Codying Style Guide"
    * This goes over requirments and recommended coding practices as well as:
        * Doccumentation
        * Function/File/Variable/etc naming
        * Must dos (like closing streams)
        * etc
