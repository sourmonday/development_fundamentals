## Class Scheduling Methodology

(TODO)

## Syllabus

| Week ## | Topics Class 1 (Programming Fundamentals) | Topics Class 2 (Dev Tools, Processes and Concepts)                                                                |
|---------|-----------------|---------------------------------------------------------|
| Week 00 | Introduction to C Programming Language | Version Control Software |
|  | Data Types | Intro to IDE's / Dev Environments
|  | If-else, switch-case | Code Quality Best Practices 
| | Basic loops | Problem breakdown
| Week 01 | C-style strings | Pseudo-code/problem solving
| | Loops | Basic Pointers
| Week 02 | Introduction to x86 Assembly | Introduction to Debugging
| | | Writing test cases
| Week 03 | Arrays and Structures | Code Quality Best Practices (2)
| | Implementing Classes & Functions | Object Oriented Programming |
| | C Basics Puzzle Capstone | Inheritance
| Week 04 | C File I/O | |
| | C Networking
| Week 05 | Pointers and Function Pointers | Stacks and Heaps
| | Recursion | Error Handling
| Week 06 | Algorithms with Data Structures | Applications of Algorithms (2)
| | | Stacks (programmatic) and Linked Lists
| Week 07 | Algorithms with Data Structures (2) | Trees 
| Week 08 | Algorithm Crucible | |
| Week 09 | Algorithm Crucible Review | Command Line Navigation and Usage
| | Python Crash Course | Big O Notation
| | C Standard Library Calls | Code Quality Best Practices (3)
| | | Re-factoring Legacy Code
| Week 10 | Applications of Algorithms Refresher
| | Algorithm Crucible
| Week 11 | Capstone Sprint 1 | Introduction to an Agile Team
| | Capstone Sprint 2
