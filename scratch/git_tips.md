If you have any issues with Git, try debugging the issue by doing the following:

1. On bash, run the following:

```
export GIT_TRACE_PACKET=1
export GIT_TRACE=1
export GIT_CURL_VERBOSE=1
```

Then run your command and analyze the output
